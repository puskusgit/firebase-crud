<?php

return [

    'firebase' => [
        'api_key' => "AIzaSyCwy0cSRyzwXAaVQR1-G1L6Wa5NkkUOK74",, // Only used for JS integration
        'auth_domain' => 'test-ba758.firebaseapp.com', // Only used for JS integration
        'database_url' => 'https://test-ba758.firebaseio.com',
        'secret' => 'secret',
        'storage_bucket' => "test-ba758.appspot.com", // Only used for JS integration
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

];
